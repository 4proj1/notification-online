## Qu'est-ce que c'est ?

Ceci est un template de projet symfony dockerisé. Il sert à avoir un projet déjà configuré et ainsi pouvoir se mettre directement à coder sans se soucier de la mise en production ou l'installation de l'environnement de développement.

## Comment on s'en sert ?

### 1) Créer un nouveau projet

Pour créer un nouveau projet, il faut : 
- cloner ce projet
- supprimer la remote master (git remode remove origin)
- créer un projet vide sur gitlab
- créer une remote origin grâce au projet vide précédement créé (git remote add origin <URL du projet>
- remplacer <insert image tag> dans le Makefile par le nom de votre projet (présent 4 fois)
- remplacer <project name> dans mkdocs.yml
- push le projet (git push origin <nom de la branche>)

### 2) Développer ce nouveau projet

Ce nouveau projet que vous venez de créer contiens désormais des outils pour aider le développement.

Le Makefile permet de faciliter de nombreuses choses :
- make deploy-dev : permet de créer un container symfony et un container mariadb liés ensemble. Après cette commande, votre site sera accessible à l'addresse localhost:8000
- make migration : permet de lancer une migration sur l'image symfony (console make:migration) et ainsi déployer les changements apportés à la base de données via doctrine.
- make push : construit l'image docker et la push dans le registry

/!\ ADAPTEZ CE README ET LA DOC A VOTRE PROJET /!\