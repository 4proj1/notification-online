deploy-dev:
	docker container prune
	docker volume prune
	docker-compose up

migration:
	docker exec toutpareilusermanager_usermanager_1 php /app/project/bin/console make:migration
	docker exec toutpareilusermanager_usermanager_1 php /app/project/bin/console doctrine:migrations:migrate

push:
	docker build . -t <insert image tag>
	docker image tag <insert image tag> c-est.party:5000/<insert image tag>
	docker push c-est.party:5000/<insert image tag>