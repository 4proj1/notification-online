<?php

namespace App\Controller;

use App\Entity\MessageNotification;
use App\Repository\MessageNotificationRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpClient\HttpClient;
use Psr\Http\Message\ResponseInterface;

class NotificationController extends AbstractController
{

    private static $messages = array();

    /**
     * @Route("/addnotification/{username}--{theMessage}", name="addnotification", methods={"GET","HEAD"})
     * @param $username
     * @param $theMessage
     * @return Response
     */
    public function addMessage($username, $theMessage) {

        $manager = $this->getDoctrine()->getManager();

        $res = new MessageNotification();
	    $res->setMessage($theMessage);
	    $res->setTimestamp(microtime());
	    $res->setUsername($username);
	    $manager->persist($res);
	    $manager->flush();
	    return new Response("OK");
    }

    /**
     * @Route("/notification/{username}--{timestamps}", name="notification", methods={"GET","HEAD"})
     * @param MessageNotificationRepository $messageNotificationRepository
     * @param $username
     * @param $timestamps
     * @return Response
     */
    public static function haveMessage(MessageNotificationRepository $messageNotificationRepository, $username, $timestamps) {
    	$messages = $messageNotificationRepository->findBy(["username" => $username]);

        $res = "";

        foreach ($messages as $message ) {
            if((int) explode(" ", $message->getTimestamp())[1] > $timestamps) {
                $res .= $message->getMessage()."|";
            }
        }
        return new Response(substr($res, 0, -1));
    }

    /**
     * @Route("/getmap", name="getmap", methods={"GET","HEAD"})
     */
	public static function getMap() {
    	$mapURL = $_ENV["URL_MAP"];
		$httpClient = HttpClient::create();
        $response = $httpClient->request('GET', $mapURL);
        return new Response($response->getContent());
    }
}
